var data = [];

$(document).ready(function() {
    $.getJSON("drilldown.json", function(result,status,xhr) {
        var data2016 = [];
        var data2017 = [];
        var data2018 = [];
        for (var i=0;i < result.length;i++) {
            if (result[i].year == '2016') {
                data2016.push(result[i].income);
            }else if (result[i].year == '2017') {
                data2017.push(result[i].income);
            }else {
                data2018.push(result[i].income);
            }
        }
        data.push(data2016, data2017, data2018);
        yearChart(data);
    });
});

function monthChart(j_data, q_data, year, qLabel) {

    var monthLabel = [];

    if (qLabel == 1) {
        monthLabel.push("1","2","3");
    }else if (qLabel == 2) {
        monthLabel.push("4","5","6");
    }else if (qLabel == 3) {
        monthLabel.push("7","8","9");
    }else {
        monthLabel.push("10","11","12");
    }

    var monthChartData = {
        type: 'bar',
        data: {
            labels: monthLabel,
            datasets: [{
                label: qLabel,
                backgroundColor: [
                    "rgba(255, 99, 132, 0.2)",
                    "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)"
                ],
                borderColor: [
                    "rgb(255, 99, 132)",
                    "rgb(255, 159, 64)",
                    "rgb(255, 205, 86)"
                ],
                data: q_data,
                fill: false,
                borderWidth: 1
            }]
        },
        options: {
            responsive: false,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    }

    $('#drillDownChart').remove();
    $('#toolbox').remove();
    $('.container').append('<div id="toolbox">'
                          +'<div id="crumbs">'
                          +'<ul>'
                          +'<li><a id="backYear">'+ year +'</a></li>'
                          +'<li><a id="backQuarter">Quarter '+ qLabel + '</a></li>'
                          +'</ul></div>'
                          +'<button id="back">Back</button>'
                          +'</div>'
    );
    $('.container').append('<canvas id="drillDownChart" width="800" height="450"></canvas>');
    var monthCanvas = document.getElementById('drillDownChart');
    var monthchart = new Chart(monthCanvas, monthChartData);

    $("#back").click(function(){
        quarterChart(j_data, year);
    });
    $("#backYear").click(function(){
        yearChart(j_data);
    });
    $("#backQuarter").click(function(){
        quarterChart(j_data, year);
    });
}

function quarterChart(j_data, year) {
    if (year == "2016") {
        var y_data = j_data[0];
    }else if (year == "2017") {
        var y_data = j_data[1];
    }else {
        var y_data = j_data[2];
    }

    var quarterData = [];
    for (var i = 0; i < y_data.length; i+=3) {
        quarterData.push(y_data[i]+y_data[i+1]+y_data[i+2]);
    }

    var quarterLabel = [];

    for (var t = 0; t < quarterData.length; t++) {
        var quarterName = "Quarter " + (t+1);
        quarterLabel.push(quarterName);
    }

    var quarterChartData = {
        type: 'bar',
        data: {
            labels: quarterLabel,
            datasets: [{
                label: year,
                backgroundColor: [
                    "rgba(255, 99, 132, 0.2)",
                    "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)",
                    "rgba(75, 192, 192, 0.2)"
                ],
                borderColor: [
                    "rgb(255, 99, 132)",
                    "rgb(255, 159, 64)",
                    "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)"
                ],
                data: quarterData,
                fill: false,
                borderWidth: 1
            }]
        },
        options: {
            responsive: false,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    }

    $('#drillDownChart').remove();
    $('#toolbox').remove();
    $('.container').append('<div id="toolbox">'
                          +'<div id="crumbs">'
                          +'<ul>'
                          +'<li><a id="backYear">' + year + '</a></li>'
                          +'</ul></div>'
                          +'<button id="back">Back</button>'
                          +'</div>'
    );
    $('.container').append('<canvas id="drillDownChart" width="800" height="450"></canvas>');
    var quarterCanvas = document.getElementById('drillDownChart');
    var quarterchart = new Chart(quarterCanvas, quarterChartData);

    $("#back").click(function(){
        yearChart(j_data);
    });
    $("#backYear").click(function(){
        yearChart(j_data);
    });

    quarterCanvas.onclick = function(evt) {
        var activePoint = quarterchart.getElementAtEvent(evt)[0];
        var data = activePoint._chart.data;
        var label = data.labels[activePoint._index];

        var q_data = [];

        if (label == "Quarter 1") {
            q_data.push(y_data[0], y_data[1], y_data[2]);
            var qLabel = 1;
        }else if (label == "Quarter 2") {
            q_data.push(y_data[3], y_data[4], y_data[5]);
            var qLabel = 2;
        }else if (label == "Quarter 3") {
            q_data.push(y_data[6], y_data[7], y_data[8]);
            var qLabel = 3;
        }else {
            q_data.push(y_data[9], y_data[10], y_data[11]);
            var qLabel = 4;
        }

        monthChart(j_data,q_data,year,qLabel)
    };
}

function yearChart(j_data) {
    var yearData = [];
    for (var i = 0; i < j_data.length; i++) {
        var yearResult = 0;
        for (var j = 0; j < j_data[i].length; j++) {
            yearResult = yearResult + j_data[i][j];
        }
        yearData.push(yearResult);
    }

    var yearChartData = {
        type: 'bar',
        data: {
            labels: ["2016", "2017", "2018"],
            datasets: [{
                label: "Year",
                backgroundColor: [
                    "rgba(255, 159, 64, 0.2)",
                    "rgba(75, 192, 192, 0.2)",
                    "rgba(153, 102, 255, 0.2)"
                ],
                borderColor: [
                    "rgb(255, 159, 64)",
                    "rgb(75, 192, 192)",
                    "rgb(153, 102, 255)"
                ],
                data: yearData,
                fill: false,
                borderWidth: 1
            }]
        },
        options: {
            responsive: false,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    }

    $('#drillDownChart').remove();
    $('#toolbox').remove();
    $('.container').append('<canvas id="drillDownChart" width="800" height="450"></canvas>');
    var yearCanvas = document.getElementById('drillDownChart');
    var yearchart = new Chart(yearCanvas, yearChartData);

    yearCanvas.onclick = function(evt) {
        var activePoint = yearchart.getElementAtEvent(evt)[0];
        var data = activePoint._chart.data;
        var label = data.labels[activePoint._index];

        quarterChart(j_data, label);
    };
}
